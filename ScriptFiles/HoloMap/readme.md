# Holomap

This makes a scan of an area 48x48 and creates a hologram map of the scanned area

## Usage
Just run the program. No special arguments

## Special Block Requirements
Hologram : *OpenComputers*  
Geolyzer : *OpenComputers*

## Pastebin command

```pastebin get i7RTGvff holomap```