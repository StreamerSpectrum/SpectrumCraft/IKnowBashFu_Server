# Hologram Logo
uses 2 hologram components to display the Streamer Spectrum Logo.

## Usage
```hologram_logo```  

### example  
```holosign capgames 1 10 8 0x7C91FF```

## Special Block Requirements
Hologram : *OpenComputers*

## Pastebin command
```pastebin get mg6duLQa hologram_logo```