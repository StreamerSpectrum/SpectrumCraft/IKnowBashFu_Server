local c = require("component")
local pa = c.proxy("front_hologram_address")
local pb = c.proxy("rear_hologram_address")

pa.clear()
pb.clear()

pa.setPaletteColor(1,0x240E44)
pa.setPaletteColor(2,0x27B3EB)
pa.setPaletteColor(3,0xFFFFFF)
pb.setPaletteColor(1,0x7ACFE6)
pb.setPaletteColor(2,0xACDEE3)
pb.setPaletteColor(3,0x4AC4DF)

pa.setScale(1.5)
pb.setScale(1.5)

pic = {"xxxxxxxxxxxxxxxaaxxxxxxxxxxxxxxx",
       "xxxxxxxxxxxxxaaaaaaxxxxxxxxxxxxx",
       "xxxxxxxxxxxaaaabbaaaaxxxxxxxxxxx",
       "xxxxxxxxxaaaabbbbbbaaaaxxxxxxxxx",
       "xxxxxxxxaaabbbbaabbbbaaaxxxxxxxx",
       "xxxxxxaaabbbbaaaaaabbbbaaaxxxxxx",
       "xxxxaaabbbbaaaacccaaabbbbaaaxxxx",
       "xxaaabbbbaaaaaccccccaaabbbbaaaxx",
       "xaabbbbaaaaaacccccccccaaabbbbaax",
       "xaabbdaaaaaaacccccccccccaaabbaax",
       "xaabbddaaaaaaeeeeeeeeeeeeeebbaax",
       "xaabbddddaaaaeeeeeeeeeeeeeebbaax",
       "xaabbdddddaaaeeeeeeeeeeeeeebbaax",
       "xaabbdddddddaaeeeeeeeeeeeeebbaax",
       "xaabbddddddddaaeeeeeeeeeeeebbaax",
       "xaabbdddddddddaaaeeeeeeeeeebbaax",
       "xaabbddddddddddaaaeeeeeeeeebbaax",
       "xaabbddddddddddddaaeeeeeeeebbaax",
       "xaabbdddddddddddddaaeeeeeeebbaax",
       "xaabbddddddddddddddaaaeeeeebbaax",
       "xaabbddddddddddddddaaaaeeeebbaax",
       "xaabbddddddddddddddaaaaaaeebbaax",
       "xaabbaaafffffffffffaaaaaaaebbaax",
       "xaabbbbaaafffffffffaaaaaabbbbaax",
       "xxaaabbbbaaafffffffaaaabbbbaaaxx",
       "xxxxaaabbbbaaafffaaaabbbbaaaxxxx",
       "xxxxxxaaabbbbaaaaaabbbbaaaxxxxxx",
       "xxxxxxxxaaabbbbaabbbbaaaxxxxxxxx",
       "xxxxxxxxxaaaabbbbbbaaaaxxxxxxxxx",
       "xxxxxxxxxxxaaaabbaaaaxxxxxxxxxxx",
       "xxxxxxxxxxxxxaaaaaaxxxxxxxxxxxxx",
       "xxxxxxxxxxxxxxxaaxxxxxxxxxxxxxxx"}

function colorselect(t,device)
    if device == "front" then
        if t == "a" then return 1
        elseif t == "b" then return 2
        elseif t == "c" then return 3
        else return false end
    else
        if t == "d" then return 1
        elseif t == "e" then return 2
        elseif t == "f" then return 3
        else return false end
    end
end


for p = 0,31 do
    for i= 1,32 do
        local t = string.sub(pic[p+1],i,i)
        pa.set(8+i, 32-p, 20,colorselect(t,"front"))
        pb.set(8+i, 32-p, 31,colorselect(t,"back"))
    end
end