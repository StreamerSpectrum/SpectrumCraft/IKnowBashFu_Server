# Holosign
This makes a static holographic sign depending on the text and scale and uses up to 5 paramaters to configgure to your needs

## Usage
```holosign <display_text> <scale> <height> <depth_offset> <color>```  

### paramaters
* display_text = (required) text : if more than 1 word, encase in quotes
* scale = min value: 0.33, max value: 5, _ for default
* height = min value: 5, max 32, _ for default
* depth = min and max value : 23, _ for default
* color = ```0x<rgb format>```, leave blank or use _ for default

### example  
```holosign capgames 1 10 8 0x7C91FF```

## Special Block Requirements
Hologram : *OpenComputers*  
Adapter : *OpenComputers*

## Pastebin command
```pastebin get AcA2vS5M static_holosign```