local component = require("component")
local holo = component.hologram
local keyboard = require("keyboard")

local params = {...} 
holo.clear()
-- paramaters
-- 1 = text
-- 2 = scale
-- 3 = height
-- 4 = depth offset
-- 5 = color

if params[2] == nil or params[2] == "_" then params[2] = 1 end
if params[3] == nil or params[3] == "_" then params[3] = 30 end
if params[4] == nil or params[4] == "_" then params[4] = 0 end
if params[5] == nil or params[5] == "_" then params[5] = 0xFF0000 else params[5] = tonumber(params[5]) end
 
holo.setPaletteColor(1, params[5])
holo.setScale(tonumber(params[2]))

function display_text(character,x,y,z)
    if character == 'a' then
        for h = 1,5 do
            if h == 1 or h==3 then 
                for col = 0,4 do
                    holo.set(x+col,y-h,z,1)
                end
            end
            holo.set(x,y-h,z,1)
            holo.set(x + 4,y-h,z,1)
        end
    elseif character == 'b' then
        for h = 1,5 do
            if h == 1 or h == 3 or h == 5 then
                for col = 1,3 do
                    holo.set(x+col,y-h,z,1)
                end
            end
            if h == 2 or h == 4 then holo.set(x+4,y-h,z,1) end
            holo.set(x,y-h,z,1)
        end
    elseif character == 'c' then
        for h = 1,5 do
            if h == 1 or h == 5 then
                for col = 1,4 do
                    holo.set(x +col,y-h,z,1)
                end
            end
    
            holo.set(x,y-h,z,1)
        end
    elseif character == 'd' then
        for h = 1,5 do
            if h == 1 or h == 5 then
                for col = 1,3 do
                    holo.set(x +col,y-h,z,1)
                end
            end
            if h > 1 and h < 5 then holo.set(x+4,y-h,z,1) end
            holo.set(x,y-h,z,1)
        end
    elseif character == 'e' then
        for h = 1,5 do
            if h == 1 or h == 5 then
                for col = 1,4 do
                    holo.set(x +col,y-h,z,1)
                end
            end
            if h == 3 then
                for col = 1,3 do
                    holo.set(x+col,y-h,z,1)
                end
            end
            holo.set(x,y-h,z,1)
        end
    elseif character == 'f' then
        for h = 1,5 do
            if h == 1 then
                for col = 1,4 do
                    holo.set(x+col,y-h,z,1)
                end
            end
            if h == 3 then
                for col = 1,3 do
                    holo.set(x+col,y-h,z,1)
                end
            end
            holo.set(x,y-h,z,1)
        end
    elseif character == 'g' then
        for h = 1,5 do
            if h == 1 or h == 5 then 
                for col = 0,4 do
                    holo.set(x+col,y-h,z,1)
                end
            end
            if h >=3 then holo.set(x + 4,y-h,z,1) end
            if h == 3 then
                for col = 2,4 do
                    holo.set(x+col,y-h,z,1)
                end
            end
            holo.set(x,y-h,z,1)
        end
    elseif character == 'h' then
        for h = 1,5 do
            if h==3 then 
                for col = 0,4 do
                    holo.set(x+col,y-h,z,1)
                end
            end
            holo.set(x,y-h,z,1)
            holo.set(x + 4,y-h,z,1)
        end
    elseif character == 'i' then
        for h = 1,5 do
            if h==1 or h == 5 then 
                for col = 0,4 do
                    holo.set(x+col,y-h,z,1)
                end
            end
            holo.set(x + 2,y-h,z,1)
        end   
    elseif character == 'j' then
        for h = 1,5 do
            if h == 5 then 
                for col = 0,3 do
                    holo.set(x+col,y-h,z,1)
                end
            end
            if h >3 then holo.set(x,y-h,z,1) end
            holo.set(x + 4,y-h,z,1)
        end 
    elseif character == 'k' then
        for h = 1,5 do
            if h == 1 or h==5 then 
                holo.set(x + 4,y-h,z,1)
            end
            if h == 2 or h == 4 then holo.set(x + 3 ,y-h,z,1) end
            if h == 3 then 
                for col = 0,2 do
                    holo.set(x +col,y-h,z,1)
                end
            end
            holo.set(x,y-h,z,1)
        end
    elseif character == 'l' then
        for h = 1,5 do
            if h==5 then 
                for col = 1,4 do
                    holo.set(x +col,y-h,z,1)
                end
            end
            holo.set(x,y-h,z,1)
        end
    elseif character == 'm' then
        for h = 1,5 do
            holo.set(x,y-h,z,1)
            holo.set(x + 4,y-h,z,1)
            if h == 2 then
                for col = 0,1 do
                    holo.set(x +col,y-h,z,1)
                end
                for col = 3,4 do
                    holo.set(x +col,y-h,z,1)
                end
            end
            if h == 3 then holo.set(x + 2,y-h,z,1) end
        end
    elseif character == 'n' then
        for h = 1,5 do
            holo.set(x,y-h,z,1)
            holo.set(x + 4,y-h,z,1)
            if h == 2 then holo.set(x + 1,y-h,z,1) end
            if h == 3 then holo.set(x + 2,y-h,z,1) end
            if h == 4 then holo.set(x + 3,y-h,z,1) end
        end
    elseif character == 'o' then
        for h = 1,5 do
            if h == 1 or h == 5 then 
                for col = 0,3 do
                    holo.set(x+col,y-h,z,1)
                end
            end
            holo.set(x,y-h,z,1)                
            holo.set(x + 4,y-h,z,1)
        end
    elseif character == 'p' then
        for h = 1,5 do
            if h == 1 or h == 3 then 
                for col = 0,3 do
                    holo.set(x+col,y-h,z,1)
                end
            end
            if h <=3 then holo.set(x + 4,y-h,z,1) end
            holo.set(x,y-h,z,1)
        end
    elseif character == 'q' then
        for h = 1,5 do
            if h == 1 then 
                for col = 0,4 do
                    holo.set(x+col,y-h,z,1)
                end
            end
            if h == 4 then holo.set(x+3,y-h,z,1) end
            if h == 5 then 
                for col = 0,2 do
                    holo.set(x+col,y-h,z,1)
                    holo.set(x + 4,y-h,z,1)
                end
            end
            if h < 4 then holo.set(x + 4,y-h,z,1) end
            holo.set(x,y-h,z,1)
        end
    elseif character == 'r' then
        for h = 1,5 do
            if h == 1 then 
                for col = 0,4 do
                    holo.set(x+col,y-h,z,1)
                end
            end
            if h == 3 then 
                for col = 0,3 do
                    holo.set(x+col,y-h,z,1)
                end
            end
            if h < 3 then holo.set(x+4,y-h,z,1) end
            if h > 3 then holo.set(x+4,y-h,z,1) end
            holo.set(x,y-h,z,1)
        end
    elseif character == 's' then
        for h = 1,5 do
            if h == 1 or h == 3 or h == 5 then 
                for col = 0,4 do
                    holo.set(x+col,y-h,z,1)
                end
            end
            if h <=3 then holo.set(x,y-h,z,1) end
            if h >=3 then holo.set(x+4,y-h,z,1) end
        end
    elseif character == 't' then
        for h = 1,5 do
            if h == 1 then 
                for col = 0,4 do
                    holo.set(x+col,y-h,z,1)
                end
            end
            holo.set(x+2,y-h,z,1)
        end
    elseif character == 'u' then
        for h = 1,5 do
            if h == 5 then 
                for col = 0,4 do
                    holo.set(x+col,y-h,z,1)
                end
            end
            holo.set(x,y-h,z,1)
            holo.set(x+4,y-h,z,1)
        end
    elseif character == 'v' then
        for h = 1,5 do
            if h < 4 then 
                holo.set(x,y-h,z,1)
                holo.set(x+4,y-h,z,1)
            end
            if h == 4 then
                holo.set(x,y-h,z,1)
                holo.set(x+4,y-h,z,1)
            end
            if h == 4 then
                holo.set(x+1,y-h,z,1)
                holo.set(x+3,y-h,z,1)
            end
            if h == 5 then holo.set(x+2,y-h,z,1) end
        end
    elseif character == 'w' then
        for h = 1,5 do
            if h < 5 then 
                holo.set(x,y-h,z,1)
                holo.set(x+4,y-h,z,1)
            end
            if h > 2 and h < 5 then holo.set(x+2,y-h,z,1) end
            if h == 5 then 
                holo.set(x+1,y-h,z,1) 
                holo.set(x+3,y-h,z,1)                 
            end
        end
    elseif character == 'x' then
        for h = 1,5 do
            if h == 1 or h == 5 then 
                holo.set(x,y-h,z,1)
                holo.set(x+4,y-h,z,1)
            end
            if h == 2 or h == 4 then 
                holo.set(x+1,y-h,z,1)
                holo.set(x+3,y-h,z,1)
            end
            if h == 3 then holo.set(x+2,y-h,z,1) end
        end
    elseif character == 'y' then
        for h = 1,5 do
            if h == 3 then 
                for col = 1,3 do
                    holo.set(x+col,y-h,z,1)
                end
            end
            if h < 3 then 
                holo.set(x,y-h,z,1) 
                holo.set(x+4,y-h,z,1)
            end
            if h > 2 then holo.set(x+2,y-h,z,1) end
        end
    elseif character == 'z' then
        for h = 1,5 do
            if h == 1 or h == 5 then 
                for col = 0,4 do
                    holo.set(x+col,y-h,z,1)
                end
            end
            if h == 3 then
                for col = 1,3 do
                    holo.set(x+col,y-h,z,1)
                end end
            if h == 2 then holo.set(x+4,y-h,z,1) end
            if h == 4 then holo.set(x,y-h,z,1) end
        end

    else
        print("Unknown Character: " .. character)
    end
end

function show(name)
    local p = 1
    for i = 1,string.len(name) do
        local c = string.sub(name,i,i)
        display_text(c,p, tonumber(params[3]), 24 + tonumber(params[4]))
        p = p + 6
    end
end

show(string.lower(params[1]))