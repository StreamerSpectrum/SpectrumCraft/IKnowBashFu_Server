# ProxDoor
This allows you to make a door open using a motion sensor and can be used as a security mesure.

## Usage
run the program with the following arguments

```proxdoor <seconds> <whitelist>```
an example
```proxdoor 2 CAPGames KarmaKanadian```
the whitelist is created by simplay puting each name at the end seperated by a space

## Special Block Requirements
Motion Sensor : *OpenComputers*

## Pastebin Command
```pastebin get AUtKfF4k proxdoor```