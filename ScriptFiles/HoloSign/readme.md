# Holosign
This makes a holographic sign depending on the text and scale and uses 2 paramaters to determine what tet to display and how big

## Usage
```holosign <display_text> <scale>```  
### example  
```holosign "Spectrum Research Lab" 3.7```

## Special Block Requirements
Hologram : *OpenComputers*  
Adapter : *OpenComputers*

## Pastebin command
```pastebin get vw4UVSrn holosign```