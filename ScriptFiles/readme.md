# Home Spawn Scripts

This is scrits for the main whome spawn area.

* Program Folders - All programs must be in a folder of their own.
* Prorogram readme - Each program folder must contain a readme with info about said program.
* Links - Each program Readme must contain pastebin links.
* File Format - Program files should be uploaded as .lua only

## Program Transfering

### Downloading Programs in Minecraft
To download programs into the game, use the following command at a computer

```pastbin get <code> <filename>```

### Uploading/Backing up programs
To download programs into the game, use the following command at a computer

```pastbin put <code> <filename>```

# Program Directory
filename | | code | usage
---|---|---|---
holomap | | [i7RTGvff](https://pastebin.com/i7RTGvff) | ```holomap```
proxdoor | | [AUtKfF4k](https://pastebin.com/AUtKfF4k) | ```proxdoor <seconds> <allowed list>```
holosign | | [vw4UVSrn](https://pastebin.com/vw4UVSrn) | ```holosign <display_text> <scale>```