# Companion Cube
This is a custom desk designed by CAPGames and IKnowBashFu. With 4 different segments, you can create a desk the way you want

## Printed Materials

* All components use Colored Hardened Clay 

## Image Example
![Desk](https://gitlab.com/StreamerSpectrum/SpectrumCraft/IKnowBashFu_Server/raw/master/Print3D/Desk/example.png)

## Pastebin Links
```pastebin get Pehe1dRT desk_flat_top.3dm```  
```pastebin get jknb8nVs desk_sides_full.3dm```  
```pastebin get Sk77LVq9 desk_sides_short.3dm```  
```pastebin get t6JdYxGs desk_corner.3dm```

