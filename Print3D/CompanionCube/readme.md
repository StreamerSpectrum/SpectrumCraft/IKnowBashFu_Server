# Companion Cube
This is just an example file that can be obtained though the oppm example installs. This is only serving as an example 3D Project Folder

## Printed Materials

* Edges & corners - Light Grey Wool
* Centers - Magenta Wool
* Inner - Grey Wool

## Image Example
![Companion Cube](https://gitlab.com/StreamerSpectrum/SpectrumCraft/IKnowBashFu_Server/raw/master/Print3D/CompanionCube/example.png)

## Pastebin Links
```pastebin get uSK2RdcK cc.3dm```
