# Companion Cube
This is a computer chair designed by CAPGames to compliment the Desk, this is a 2 part item

## Printed Materials

* Seat & Back = hardened_clay_stained_{color}
* Pillow =  quartz_block_bottom
* Arms & Feet = coal_block
* Base = quartz_block_bottom & coal_block
* All components use Colored Hardened Clay 

## Image Example
![Computer Chair](https://gitlab.com/StreamerSpectrum/SpectrumCraft/IKnowBashFu_Server/raw/master/Print3D/ComputerChair/example.png)

## Pastebin Links
```pastebin get LEkAETEv computer_chair_upper.3dm```  
```pastebin get Nii7dHKd computer_chair_lower.3dm```  