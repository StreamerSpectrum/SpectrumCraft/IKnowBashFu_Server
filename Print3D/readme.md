# 3D Printer

All files relating to 3d printing

# Folder Structure

* Project Folders - All 3D projects must be in a folder of their own
* Project readme - Each project folder must contain a readme with info about the project
* Links - Each project Readme must contain pastebin links for each component of the project.
* File Format - 3D printer files should be uploaded as .3dm

# Optional Additional Files
* Image - You can upload an image of the 3d object so show off your creation

# Resources
* [List of textures](https://github.com/F0x06/OpenComputers-3D-Designer/tree/master/img/resourcepacks/Vanilla/blocks)