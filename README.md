# SpectrumCraft
SpectrumCraft is basically a minecraft server with mods. This is hosted by [IKnowBashFu](https://beam.pro/iknowbashfu) from [StreamerSpectrum](https://beam.pro/team/streamerspectrum) Beam Team. 

# Setup/Mods/Server
Like with most minecraft servers, having mods is required but do not worry, we have put together a modpack for you to get started faser. please chec ServerInfo folder for more details.

# Script Files
The script files are programs that can be run on computers within the game. We have pastebin links for each file so we can easily download the code in game saving us time to copy between computer. More details inside the ScriptFiles folder.

# 3D Printer
Yes we have a 3D Printer in the game which gives us infinite possibilities for object creation. Making 3D object for the game takes time so making backups of your projects is important. All projects and more info can be found in Print3D.