# Server Requirements
In order to play on the server you will need certain mods installed.

## Minecraft Forge 
You can download the forge-1.10.2-12.18.3.2281-installer.jar (for windows) here or you can download the installer from the [Minecraft Forge website here](http://files.minecraftforge.net/maven/net/minecraftforge/forge/index_1.10.2.html)

## Mod Pack
On top of needing Minecraft Forge, you will need some other mod files. We have provided a mod pack here for you to extract into your minecraft mods foler. The Modpack is subject to change and suggestions of mod additions are welcome within reason.

## Texture Pack
We currently have no texture pack at this moment of time, however, there may be a custom one made at some point. Main builds are creaded with vanilla textures.

### Mod List
* Open Computers
* Open Glasses
* Open Radio
* Computronics
* Chisels & Bits
* Chisels & Bytes
* EnderIO
* MC Multipart
* JustEnoughItems
* RailCraft
* Super Circuit Maker

## Installing the mods
To install the mod pack its really easy. Downlaod mod_pack.zip and extract it to ```%appdata%\.minecraft\mods``` folder do the mod folder looks like this: [Mod Folder](http://i.imgur.com/ImLoO2J.png)

## Launcher Config
Load up the Minecaft launcher and log in. When logged in, 
* Select "Launch options"
* Click Add new
    * Gice the configuration a name
    * Change version to "release 1.10.2-forge1.10.2-12.18.2.2281" (last on the list)
    * (Optional) set your resolution
    * Click Save
    * You should now have: [Minecraft: Launch options](http://i.imgur.com/BNDJzgL.png)
* Click News
* Click the arrow up, next to the play button: [Minecraft: Launch selection](http://i.imgur.com/VK2ET4s.png?1)
* Select the name configuration and click play

### Multiplayer Address:  minecraft.iknowbashfu.com:25565